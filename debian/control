Source: gromacs
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Nicholas Breen <nbreen@debian.org>
Build-Depends: chrpath,
               cmake (>= 3.13),
               debhelper-compat (= 13),
               gcc-11 [!mipsel !mips64el] | gcc-9 [mipsel mips64el],
               g++-11 [!mipsel !mips64el] | g++-9 [mipsel mips64el],
               libblas-dev,
               libboost-dev,
               libfftw3-dev,
               libhwloc-dev,
               liblapack-dev,
               libx11-dev,
               lsb-release,
               mpi-default-bin,
               mpi-default-dev,
               zlib1g-dev
Build-Depends-Indep: doxygen,
                     ghostscript,
                     graphicsmagick,
                     graphviz,
                     mscgen,
                     python3-sphinx,
                     rdfind,
                     symlinks,
                     texlive-fonts-recommended,
                     texlive-latex-base,
                     texlive-latex-extra
Rules-Requires-Root: no
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debichem-team/gromacs
Vcs-Git: https://salsa.debian.org/debichem-team/gromacs.git
Homepage: https://www.gromacs.org/

Package: gromacs
Architecture: any
Depends: gromacs-data (= ${source:Version}),
         sse2-support [i386],
         sse4.2-support [amd64 x32],
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: cpp
Suggests: pymol
Description: Molecular dynamics simulator, with building and analysis tools
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 It is primarily designed for biochemical molecules like proteins and lipids
 that have a lot of complicated bonded interactions, but since GROMACS is
 extremely fast at calculating the nonbonded interactions (that usually
 dominate simulations) many groups are also using it for research on non-
 biological systems, e.g. polymers.

Package: gromacs-data
Architecture: all
Multi-Arch: foreign
Depends: libjs-mathjax, ${misc:Depends}
Recommends: gromacs
Description: GROMACS molecular dynamics sim, data and documentation
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 It is primarily designed for biochemical molecules like proteins and lipids
 that have a lot of complicated bonded interactions, but since GROMACS is
 extremely fast at calculating the nonbonded interactions (that usually
 dominate simulations) many groups are also using it for research on non-
 biological systems, e.g. polymers.
 .
 This package contains architecture-independent topology and force field
 data, documentation, man pages, and example files.

Package: libgromacs6
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: GROMACS molecular dynamics sim, shared libraries
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 It is primarily designed for biochemical molecules like proteins and lipids
 that have a lot of complicated bonded interactions, but since GROMACS is
 extremely fast at calculating the nonbonded interactions (that usually
 dominate simulations) many groups are also using it for research on non-
 biological systems, e.g. polymers.
 .
 This package contains the shared library, libgromacs.

Package: libgromacs-dev
Architecture: any
Section: libdevel
Depends: fftw3-dev,
         libgromacs6 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gromacs-data
Suggests: mpi-default-bin,
          mpi-default-dev,
          libx11-dev,
          zlib1g-dev
Description: GROMACS molecular dynamics sim, development kit
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 It is primarily designed for biochemical molecules like proteins and lipids
 that have a lot of complicated bonded interactions, but since GROMACS is
 extremely fast at calculating the nonbonded interactions (that usually
 dominate simulations) many groups are also using it for research on non-
 biological systems, e.g. polymers.
 .
 This package contains header files and static libraries for development
 purposes, plus sample Makefiles.  Development components for MPI-enabled
 GROMACS builds also require their respective packages.

Package: libnblib0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libgromacs6 (<< 2021~beta2-2)
Replaces: libgromacs6 (<< 2021~beta2-2)
Description: GROMACS molecular dynamics sim, NB-LIB shared libraries
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 The goal of NB-LIB is to enable researchers to programmatically define
 molecular simulations. Traditionally these have been performed using a
 collection of executables and a manual workflow followed by a “black-box”
 simulation engine. NB-LIB allows users to script a variety of novel
 simulation and analysis workflows at a more granular level.
 .
 This package contains the shared library, libnblib.

Package: libnblib-dev
Architecture: any
Section: libdevel
Depends: libnblib0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libgromacs-dev (= ${binary:Version}),
Suggests: gromacs (= ${binary:Version}) | gromacs-mpi (= ${binary:Version})
Breaks: libgromacs-dev (<< 2021~beta2-2)
Replaces: libgromacs-dev (<< 2021~beta2-2)
Description: GROMACS molecular dynamics sim, NB-LIB development kit
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 The goal of NB-LIB is to enable researchers to programmatically define
 molecular simulations. Traditionally these have been performed using a
 collection of executables and a manual workflow followed by a “black-box”
 simulation engine. NB-LIB allows users to script a variety of novel
 simulation and analysis workflows at a more granular level.
 .
 This package contains header files for NB-LIB.  For the classical GROMACS
 API, see libgromacs-dev.

Package: gromacs-mpi
Architecture: any
Depends: mpi-default-bin,
         sse2-support [i386],
         sse4.2-support [amd64 x32],
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gromacs
Suggests: gromacs-data
Breaks: gromacs-mpich (<< 2021.1-2), gromacs-openmpi (<< 2021.1-2)
Replaces: gromacs-mpich (<< 2021.1-2), gromacs-openmpi (<< 2021.1-2)
Description: Molecular dynamics sim, binaries for MPI parallelization
 GROMACS is a versatile package to perform molecular dynamics, i.e. simulate
 the Newtonian equations of motion for systems with hundreds to millions of
 particles.
 .
 It is primarily designed for biochemical molecules like proteins and lipids
 that have a lot of complicated bonded interactions, but since GROMACS is
 extremely fast at calculating the nonbonded interactions (that usually
 dominate simulations) many groups are also using it for research on non-
 biological systems, e.g. polymers.
 .
 This package contains only the core simulation engine with parallel
 support using the default MPI interface on the chosen architecture. It is
 suitable for nodes of a processing cluster, or for multiprocessor machines.
