#!/usr/bin/make -f
#
# build script for GROMACS

PACKAGE := gromacs

DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_DISTRO         ?= $(shell lsb_release -si)
DEB_VERSION        ?= $(shell dpkg-parsechangelog -S version)

# add hardening flags, using dpkg-buildflags
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
#653916 and http://www.cmake.org/Bug/view.php?id=12928 and https://gitlab.kitware.com/cmake/cmake/-/issues/12928
CFLAGS   += $(CPPFLAGS)
CXXFLAGS += $(CPPFLAGS)

# DEB_BUILD_OPTIONS boilerplate.
ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g -Wall
endif
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
endif
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	MAKE += -j$(NUMJOBS)
endif

# General configuration options used for all builds.
COMMON_CONFIG_PARAMS = \
	$(CURDIR) \
	-DCMAKE_VERBOSE_MAKEFILE=ON \
	-DCMAKE_RULE_MESSAGES=OFF \
	-DCMAKE_INSTALL_PREFIX="/usr" \
	-DCMAKE_EXE_LINKER_FLAGS="$(LDFLAGS)" \
	-DCMAKE_SKIP_RPATH=ON \
	-DGMX_EXTERNAL_ZLIB=ON \
	-DGMX_VERSION_STRING_OF_FORK="$(DEB_DISTRO)-$(DEB_VERSION)" \
	-DGMX_GIT_VERSION_INFO=OFF \
	-DGMX_HWLOC=ON \
	-DGMXAPI=OFF

# Specific options for the MPI-enabled builds.
MPI_CONFIG_PARAMS = \
	-DGMX_MPI=ON \
	-DGMX_X11=OFF \
	-DGMX_DEFAULT_SUFFIX=OFF \
	-DMPIEXEC="/usr/bin/mpiexec" \
	-DGMX_BUILD_MDRUN_ONLY=ON

# Temporary workaround for some GCC quirks.  The default version at the
# time of writing is 10.2.1-6, which triggers an ICE during compilation.
# 10.3 fixes the bug but is only in experimental.  The available versions
# of both GCC 9 and 11 work, but 11 is not available on all architectures
# yet.  Sooooooo, this mess chooses 11 if available, 9 if not.
#
# All of this will disappear the instant that either GCC 10.3 hits unstable,
# or GCC 11 becomes available on all release architectures including mips*.
ifneq (,$(wildcard /usr/bin/gcc-11))
	DEB_GCC=/usr/bin/gcc-11
	DEB_GPP=/usr/bin/g++-11
else ifneq (,$(wildcard /usr/bin/gcc-9))
	DEB_GCC=/usr/bin/gcc-9
	DEB_GPP=/usr/bin/g++-9
else
	# fallback to something that hopefully works
	DEB_GCC=/usr/bin/gcc
	DEB_GPP=/usr/bin/g++
endif

# For compatibility reasons, disable SIMD CPU optimizations EXCEPT
# for SSE2 on x86, SSE4.1 on amd64/x32, and NEON on arm64.
# For local compilations, set DEB_BUILD_OPTIONS=cpuopt for automatic
# detection of the best available option.
# See https://manual.gromacs.org/documentation/current/user-guide/mdrun-performance.html#intra-core-parallelization
# and https://manual.gromacs.org/documentation/current/install-guide/index.html#gmx-simd-support
ifeq (,$(findstring cpuopt,$(DEB_BUILD_OPTIONS)))
ifeq ($(DEB_HOST_ARCH), i386)
	COMMON_CONFIG_PARAMS += -DGMX_SIMD=SSE2
else ifeq ($(DEB_HOST_ARCH), amd64)
	COMMON_CONFIG_PARAMS += -DGMX_SIMD=SSE4.1
else ifeq ($(DEB_HOST_ARCH), x32)
	COMMON_CONFIG_PARAMS += -DGMX_SIMD=SSE4.1
else ifeq ($(DEB_HOST_ARCH), arm64)
	COMMON_CONFIG_PARAMS += -DGMX_SIMD=ARM_NEON_ASIMD
else
	COMMON_CONFIG_PARAMS += -DGMX_SIMD=None
endif
endif

# Include "gpu" in DEB_BUILD_OPTIONS to build GPU-accelerated binaries as well.
# This build option is not always well tested, please send comments.
# You must have the pacakges "nvidia-cuda-toolkit" and "nvidia-cuda-dev"
# installed. Those are not official build dependencies to keep the package in
# Debian main!
#
# If you use this option, it is strongly recommended that you also set
# DEB_BUILD_OPTIONS=cpuopt, assuming you are compiling and running on the same
# machine.
#
# Further details at
# https://manual.gromacs.org/documentation/current/user-guide/mdrun-performance.html#running-mdrun-with-gpus
GPU_CONFIG_PARAMS =
ifneq (,$(findstring gpu,$(DEB_BUILD_OPTIONS)))
	GPU_CONFIG_PARAMS += -DGMX_GPU=ON
endif

# Tests on RISC-V do not *build* - ICEs while compiling the embedded version
# of libtinyxml.  For the time being, bypass entirely; difficult to diagnose
# further without a riscv64 porterbox.
ifeq ($(DEB_HOST_ARCH), riscv64)
	DEB_BUILD_OPTIONS += nocheck
endif

# Improve display of build-time tests.
export GTEST_COLOR=no
export CTEST_OUTPUT_ON_FAILURE=1

#######################################################################

build: build-arch build-indep

build-arch: configure-stamp build-basic build-mpi
build-indep: build-manual

configure: configure-stamp
configure-stamp:
	dh_testdir
	(mkdir -p build/basic; cd build/basic; CC=$(DEB_GCC) CXX=$(DEB_GPP) cmake \
	$(COMMON_CONFIG_PARAMS) $(GPU_CONFIG_PARAMS) -DGMX_MPI=OFF -DGMX_X11=ON -DGMX_INSTALL_LEGACY_API=ON )
	(mkdir -p build/basic-dp; cd build/basic-dp; CC=$(DEB_GCC) CXX=$(DEB_GPP) cmake \
	$(COMMON_CONFIG_PARAMS) -DGMX_MPI=OFF -DGMX_X11=ON -DGMX_DOUBLE=ON)
	(mkdir -p build/mpi; cd build/mpi; CC=/usr/bin/mpicc CXX=/usr/bin/mpicxx cmake \
	$(COMMON_CONFIG_PARAMS) $(MPI_CONFIG_PARAMS) $(GPU_CONFIG_PARAMS) -DGMX_BINARY_SUFFIX="_mpi" -DGMX_LIBS_SUFFIX="_mpi")
	(mkdir -p build/mpi-dp; cd build/mpi-dp; CC=/usr/bin/mpicc CXX=/usr/bin/mpicxx cmake \
	$(COMMON_CONFIG_PARAMS) $(MPI_CONFIG_PARAMS) -DGMX_DOUBLE=ON -DGMX_BINARY_SUFFIX="_mpi_d" -DGMX_LIBS_SUFFIX="_mpi_d")
	touch $@

# Standard builds, in single and double precision.
build-basic: configure-stamp
	dh_testdir
	$(MAKE) -C build/basic
	$(MAKE) -C build/basic-dp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C build/basic tests
	(cd build/basic;    LD_LIBRARY_PATH=$(CURDIR)/build/basic/lib    ctest -V || dpkg-architecture -i hurd-i386 )
	$(MAKE) -C build/basic-dp tests
	(cd build/basic-dp; LD_LIBRARY_PATH=$(CURDIR)/build/basic-dp/lib ctest -V || dpkg-architecture -i hurd-i386 )
endif
	touch $@

# MPI-enabled build.
build-mpi: configure-stamp
	dh_testdir
	$(MAKE) -C build/mpi
	$(MAKE) -C build/mpi-dp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# rsh_agent: https://bugs.debian.org/494046
	# oversubscribe: https://bugs.debian.org/850229#51
	# Applicable only to OpenMPI, but no-ops on MPICH architectures
	$(MAKE) -C build/mpi tests
	(cd build/mpi ; LD_LIBRARY_PATH=$(CURDIR)/build/mpi/lib \
                        OMPI_MCA_plm_rsh_agent=/bin/false \
                        OMPI_MCA_rmaps_base_oversubscribe=1 \
                        ctest -V || dpkg-architecture -i hurd-i386 || dpkg-architecture -i x32 )
endif
	touch $@

# Documentation.
build-manual:
	dh_testdir
	# Build speedy, stripped-down version just for documentation generation.
	# Derived from (former) admin/build-docs.sh
	(mkdir -p build/documentation ; cd build/documentation; LD_LIBRARY_PATH=$(CURDIR)/build/documentation/lib \
	cmake $(CURDIR) -DCMAKE_BUILD_TYPE=Debug -DGMX_OPENMP=OFF -DGMX_SIMD=None \
	-DGMX_GPU=OFF -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_RULE_MESSAGES=OFF -DGMX_BUILD_MANUAL=ON \
	-DGMX_BUILD_HELP=ON -DGMX_VERSION_STRING_OF_FORK="$(DEB_DISTRO)-$(DEB_VERSION)")
	$(MAKE) gmx -C build/documentation
	# Generate PDF
	$(MAKE) manual -C build/documentation
	# Generate man pages
	$(MAKE) man -C build/documentation
	# Generate HTML
	$(MAKE) webpage -C build/documentation

# install-basic and install-mpi fire before the main "install" target.
install-basic: build-arch
	dh_testdir
	dh_testroot
	dh_prep -pgromacs
	dh_installdirs -pgromacs

	$(MAKE) -C build/basic install DESTDIR=$(CURDIR)/debian/gromacs
	$(MAKE) -C build/basic-dp install DESTDIR=$(CURDIR)/debian/gromacs

	dh_installman -pgromacs debian/man/demux.1 debian/man/xplor2gmx.1
	# gmx(1) -> gmx_d(1)
	dh_link -pgromacs -pgromacs-data

install-mpi: build-mpi
	dh_testdir
	dh_testroot
	dh_prep -pgromacs-mpi
	dh_installdirs -pgromacs-mpi

	$(MAKE) -C build/mpi    install DESTDIR=$(CURDIR)/debian/gromacs-mpi
	$(MAKE) -C build/mpi-dp install DESTDIR=$(CURDIR)/debian/gromacs-mpi

	# overlapping include files with libgromacs-dev: can probably remove when old API is removed
	rm -rf $(CURDIR)/debian/gromacs-mpi/usr/include

	chmod 644 $(CURDIR)/debian/gromacs-mpi/usr/bin/gmx-completion*
	mv $(CURDIR)/debian/gromacs-mpi/usr/bin/gmx-completion-mdrun_mpi.bash $(CURDIR)/debian/gromacs-mpi/usr/share/bash-completion/completions/mdrun_mpi
	mv $(CURDIR)/debian/gromacs-mpi/usr/bin/gmx-completion-mdrun_mpi_d.bash $(CURDIR)/debian/gromacs-mpi/usr/share/bash-completion/completions/mdrun_mpi_d

	dh_installman -pgromacs-mpi debian/man/mdrun_mpi.1
	dh_link -pgromacs-mpi usr/share/man/man1/mdrun_mpi.1.gz usr/share/man/man1/mdrun_mpi_d.1.gz
	dh_lintian -pgromacs-mpi

# Rules in this target largely handle the library packages and arch-indep data (but not documentation).
install: build-arch install-basic install-mpi
	dh_testdir
	dh_testroot
	dh_prep -pgromacs-data -plibgromacs-dev -plibnblib-dev
	dh_installdirs -pgromacs-data

	mv $(CURDIR)/debian/gromacs/usr/share/gromacs/top \
	   $(CURDIR)/debian/gromacs-data/usr/share/gromacs
	mv $(CURDIR)/debian/gromacs/usr/share/man \
	   $(CURDIR)/debian/gromacs-data/usr/share/man

	# concatenate bash completions; not just "cat * >" because they don't end in newlines
	perl -lne 'print' $(CURDIR)/debian/gromacs/usr/bin/gmx*.bash > \
	                  $(CURDIR)/debian/gromacs-data/usr/share/bash-completion/completions/gmx
	rm -f $(CURDIR)/debian/gromacs/usr/bin/GMXRC* $(CURDIR)/debian/gromacs/usr/bin/gmx*.bash

	# symlinks, primarily for /usr/share/doc/{gromacs,libgromacs*} -> gromacs-data
	dh_link -pgromacs-data

	# rename a pair of Perl scripts to drop the .pl extension (Policy 10.4)
	mv $(CURDIR)/debian/gromacs/usr/bin/demux.pl     $(CURDIR)/debian/gromacs/usr/bin/demux
	mv $(CURDIR)/debian/gromacs/usr/bin/xplor2gmx.pl $(CURDIR)/debian/gromacs/usr/bin/xplor2gmx

	mkdir -p $(CURDIR)/debian/libgromacs6/usr/lib/$(DEB_HOST_MULTIARCH)
	mkdir -p $(CURDIR)/debian/libnblib0/usr/lib/$(DEB_HOST_MULTIARCH)
	mv $(CURDIR)/debian/gromacs/usr/lib/*/libnb*.so.* $(CURDIR)/debian/libnblib0/usr/lib/$(DEB_HOST_MULTIARCH)/
	mv $(CURDIR)/debian/gromacs/usr/lib/*/*.so.* $(CURDIR)/debian/libgromacs6/usr/lib/$(DEB_HOST_MULTIARCH)/

	# Move all -dev material to libgromacs-dev at first, then pick out subpackages one by one.
	dh_installdirs -plibgromacs-dev
	mv $(CURDIR)/debian/gromacs/usr/include $(CURDIR)/debian/libgromacs-dev/usr/include
	# catch normal libraries plus all applicable MPI variants
	mkdir -p $(CURDIR)/debian/libgromacs-dev/usr/lib/$(DEB_HOST_MULTIARCH)
	mv $(CURDIR)/debian/gromacs/usr/lib/$(DEB_HOST_MULTIARCH)/*.so \
	   $(CURDIR)/debian/gromacs/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig \
	   $(CURDIR)/debian/libgromacs-dev/usr/lib/$(DEB_HOST_MULTIARCH)/
	rmdir $(CURDIR)/debian/gromacs/usr/lib/$(DEB_HOST_MULTIARCH) $(CURDIR)/debian/gromacs/usr/lib

	# Extract libnblib.  No pkgconfig or cmake helper files yet.
	dh_installdirs -plibnblib-dev
	mkdir -p $(CURDIR)/debian/libnblib-dev/usr/lib/$(DEB_HOST_MULTIARCH)
	mv $(CURDIR)/debian/libgromacs-dev/usr/lib/$(DEB_HOST_MULTIARCH)/libnblib.so \
	   $(CURDIR)/debian/libnblib-dev/usr/lib/$(DEB_HOST_MULTIARCH)
	mv $(CURDIR)/debian/libgromacs-dev/usr/include/nblib \
	   $(CURDIR)/debian/libnblib-dev/usr/include/nblib

	# Extract libgmxapi here in the future: work in progress

	# Back to core gromacs components.
	mv $(CURDIR)/debian/gromacs/usr/share/gromacs/template $(CURDIR)/debian/libgromacs-dev/usr/share/gromacs/template
	# remaining documentation catch-all
	mv $(CURDIR)/debian/gromacs/usr/share/gromacs/* $(CURDIR)/debian/gromacs-data/usr/share/doc/gromacs
	-rmdir $(CURDIR)/debian/gromacs/usr/share/gromacs

	# what are you doing here?
	mv $(CURDIR)/debian/gromacs/usr/share/cmake $(CURDIR)/debian/libgromacs-dev/usr/share/gromacs/
	dh_lintian -pgromacs -pgromacs-data -plibgromacs6 -plibnblib0

install-manual: build-manual install
	dh_testdir
	dh_testroot
	dh_install -pgromacs-data
	dh_installman -pgromacs-data $(CURDIR)/build/documentation/docs/man/*.[1-9]
	# dh_doxygen expects a different location of index.html, so perform its function manually.
	-find $(CURDIR)/debian/gromacs-data/usr/share/doc/gromacs/html/doxygen/ -type f -a \( -name '*.md5' -o -name '*.map' \) -delete
	rdfind -outputname /dev/null -makesymlinks true $(CURDIR)/debian/gromacs-data/usr/share/doc/
	symlinks -r -s -c $(CURDIR)/debian/gromacs-data/usr/share/doc/

binary-indep: build-arch build-indep install install-manual
	dh_testdir
	dh_testroot
	dh_installchangelogs -i
	dh_installdocs -pgromacs-data
	dh_strip_nondeterminism -i
	dh_compress -i -X.pdf
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build-arch install
	dh_testdir -a
	dh_testroot -a
	dh_installchangelogs -a
	dh_installdocs -a
	dh_strip_nondeterminism -a
	dh_compress -a
	dh_fixperms -a
	dh_dwz -a
	dh_strip -A
	dh_makeshlibs -a
	dh_shlibdeps -plibgromacs6 -L libgromacs6 -l debian/libgromacs6/usr/lib
	dh_shlibdeps -plibnblib0 -L libnblib0 -l debian/libnblib0/usr/lib:debian/libgromacs6/usr/lib
	dh_shlibdeps -pgromacs -L libgromacs6 -l debian/libgromacs6/usr/lib:debian/gromacs/usr/lib
	dh_shlibdeps -pgromacs-mpi -L gromacs-mpi -l debian/gromacs-mpi/usr/lib
	dh_gencontrol -a
	dh_installdeb -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch

clean:
	dh_testdir
	dh_testroot
	rm -rf build docs/doxygen/*.pyc
	dh_clean build-basic build-mpi build-manual configure-stamp

.PHONY: binary binary-arch binary-indep build clean install install-basic install-mpi install-manual
